﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComUbi.COMMON.Entidades
{
    public class Ubicacion:BaseDTO
    {
        public float Latitud { get; set; }
        public float Longitud { get; set; }
        public Ruta RutaUbicacion { get; set; }
        public ObjectId Id_Chofer { get; set; }
    }
}
