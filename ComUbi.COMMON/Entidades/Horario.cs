﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComUbi.COMMON.Entidades
{
    public class Horario:BaseDTO
    {
        public ObjectId Id_Chofer { get; set; }
        public string Ruta { get; set; }
        public DateTime FechaHorario { get; set; }
        public DateTime HoraSalida { get; set; }
        public bool CompartirUbicacion { get; set; }
    }
}
