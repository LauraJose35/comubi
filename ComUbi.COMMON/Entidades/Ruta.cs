﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComUbi.COMMON.Entidades
{
    public class Ruta : BaseDTO
    {
        public ObjectId Id_Chofer { get; set; }
        public string Estado { get; set; }
        public string Municipio { get; set; }
        public string Localidad { get; set; }
        public string LugarSalida { get; set; }
        public string LugarLlegada { get; set; }
    }
}
