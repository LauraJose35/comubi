﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComUbi.COMMON.Entidades
{
    public class Chofer:BaseDTO
    {
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string NombreUsuario { get; set; }
        public string Password { get; set; }
        public string Placas { get; set; }
        public string NumeroTransporte { get; set; }
        public string NumeroCelular { get; set; }
    }
}
