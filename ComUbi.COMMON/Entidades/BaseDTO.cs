﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComUbi.COMMON.Entidades
{
    public class BaseDTO
    {
        public ObjectId Id { get; set; }
        public DateTime Fecha { get; set; }
    }
}
