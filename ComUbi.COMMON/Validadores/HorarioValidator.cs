﻿using ComUbi.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComUbi.COMMON.Validadores
{
    public class HorarioValidator:AbstractValidator<Horario>
    {
        public HorarioValidator()
        {
            RuleFor(p => p.CompartirUbicacion).NotNull().NotEmpty();
            RuleFor(p => p.FechaHorario).NotNull().NotEmpty();
            RuleFor(p => p.HoraSalida).NotNull().NotEmpty();
            RuleFor(p => p.Id_Chofer).NotNull().NotEmpty();
            RuleFor(p => p.Ruta).NotNull().NotEmpty();
        }
    }
}
