﻿using ComUbi.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComUbi.COMMON.Validadores
{
    public class UbicacionValidator:AbstractValidator<Ubicacion>
    {
        public UbicacionValidator()
        {
            RuleFor(p => p.Latitud).NotNull().NotEmpty();
            RuleFor(p => p.Longitud).NotNull().NotEmpty();
            RuleFor(p => p.RutaUbicacion).NotNull().NotEmpty();
            RuleFor(p => p.Id_Chofer).NotNull().NotEmpty();
        }
    }
}
