﻿using ComUbi.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComUbi.COMMON.Validadores
{
    public class RutaValidator:AbstractValidator<Ruta>
    {
        public RutaValidator()
        {
            RuleFor(p => p.Estado).NotNull().NotEmpty();
            RuleFor(p => p.Id_Chofer).NotNull().NotEmpty();
            RuleFor(p => p.Localidad).NotNull().NotEmpty();
            RuleFor(p => p.LugarLlegada).NotNull().NotEmpty();
            RuleFor(p => p.LugarSalida).NotNull().NotEmpty();
            RuleFor(p => p.Municipio).NotNull().NotEmpty();
        }
    }
}
