﻿using ComUbi.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComUbi.COMMON.Validadores
{
    public class ChoferValidator:AbstractValidator<Chofer>
    {
        public ChoferValidator()
        {
            RuleFor(p=>p.Apellidos).NotNull().NotEmpty();
            RuleFor(p => p.NombreUsuario).NotNull().NotEmpty();
            RuleFor(p => p.Nombres).NotNull().NotEmpty();
            RuleFor(p => p.NumeroCelular).NotNull().NotEmpty();
            RuleFor(p => p.NumeroTransporte).NotNull().NotEmpty();
            RuleFor(p => p.Password).NotNull().NotEmpty();
            RuleFor(p => p.Placas).NotNull().NotEmpty();
        }
    }
}
