﻿using ComUbi.COMMON.Entidades;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComUbi.COMMON.Interfaces
{
    public interface IGenericRepository<T> where T:BaseDTO
    {
        T Create(T Entidad);
        bool Delete(ObjectId Id);
        T Update(T Entidad);
        IEnumerable<T> Read { get; }
        String Error { get; }
    }
}
