﻿using ComUbi.COMMON.Entidades;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComUbi.COMMON.Interfaces
{
    public interface IGenericManager<T> where T:BaseDTO
    {
        T Agregar(T Entidad);
        bool Eliminar(ObjectId Id);
        T Actualizar(T entidad);
        IEnumerable<T> Leer { get; }
        string Error { get; }
    }
}
