﻿using ComUbi.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComUbi.COMMON.Interfaces
{
    public interface IChoferManager:IGenericManager<Chofer>
    {
    }
}
