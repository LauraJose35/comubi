﻿using ComUbi.COMMON.Entidades;
using ComUbi.COMMON.Interfaces;
using MongoDB.Bson;
using MongoDB.Driver;
using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComUbi.DAL
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseDTO
    {
        MongoClient client;
        IMongoDatabase db;
        bool resultado;
        public string Error { get; private set; }
        AbstractValidator<T> validador;
        ValidationResult resultadoValidador;
        public GenericRepository(AbstractValidator<T> _validador)
        {
            client = new MongoClient("mongodb+srv://user:user7@2%689@cluster0-x7oe8.mongodb.net/test?retryWrites=true&w=majority");
            db = client.GetDatabase("ComUbi");
            validador = _validador;
        }

        private IMongoCollection<T> Collection() => db.GetCollection<T>(typeof(T).Name);

        public IEnumerable<T> Read
        {
            get
            {
                try
                {
                    Error = "";
                    return Collection().AsQueryable();
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return null;
                }
            }
        }
                
        public T Create(T Entidad) 
        { 
        Entidad.Id = ObjectId.GenerateNewId();
        Entidad.Fecha = DateTime.Now;
         
            resultadoValidador = validador.Validate(Entidad);
            if (resultadoValidador.IsValid)
            {
                try
                {
                    if (resultadoValidador.IsValid)
                    {
                        Collection().InsertOne(Entidad);
                        Error = "";
                        resultado = true;
                    }
                    else
                    {
                        resultado = false;
                    }
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    resultado = false;
                }
            }
            else
            {
                Error = "Datos Invalidos:";
                foreach (var error in resultadoValidador.Errors)
                {
                    Error += $"\n>{error.ErrorMessage}";
                }
            }
            return resultado? Entidad : null;
        }

        public bool Delete(ObjectId Id)
        {
            try
            {
                int r = (int)Collection().DeleteOne(p => p.Id == Id).DeletedCount;
                resultado = r == 1;
                Error = "";
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                resultado = false;
            }
            return resultado;
        }

        public T Update(T Entidad)
        {
            if (validador.Validate(Entidad).IsValid)
            {
                try
                {
                    int r = (int)Collection().ReplaceOne(p => p.Id == Entidad.Id, Entidad).ModifiedCount;
                    resultado = r == 1;
                    Error = "";
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    resultado = false;
                }
            }
            return resultado ? Entidad : null;
        }
    }
}
