﻿using ComUbi.COMMON.Entidades;
using ComUbi.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComUbi.BIZ
{
    public class RutaManager : GenericManager<Ruta>, IRutaManager
    {
        public RutaManager(IGenericRepository<Ruta> _repository) : base(_repository)
        {
        }
    }
}
