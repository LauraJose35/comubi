﻿using ComUbi.COMMON.Entidades;
using ComUbi.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComUbi.BIZ
{
    public class ChoferManager : GenericManager<Chofer>, IChoferManager
    {
        public ChoferManager(IGenericRepository<Chofer> _repository) : base(_repository)
        {
        }
    }
}
