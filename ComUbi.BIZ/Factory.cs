﻿using ComUbi.COMMON.Entidades;
using ComUbi.COMMON.Validadores;
using ComUbi.DAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComUbi.BIZ
{
    public class Factory
    {
        public ChoferManager ChoferManager => new ChoferManager(new GenericRepository<Chofer>(new ChoferValidator()));
        public HorarioManager HorarioManager => new HorarioManager(new GenericManager<Horario>(new HorarioValidator()));
        public RutaManager RutaManager => new RutaManager(new GenericManager<Ruta>(new RutaValidator()));
        public UbicacionManager UbicacionManager => new UbicacionManager(new GenericManager<Ubicacion>(new UbicacionValidator()));
    }
}
