﻿using ComUbi.COMMON.Entidades;
using ComUbi.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ComUbi.BIZ
{
    public class UbicacionManager : GenericManager<Ubicacion>, IUbicacionManager
    {
        public UbicacionManager(IGenericRepository<Ubicacion> _repository) : base(_repository)
        {
        }
    }
}
