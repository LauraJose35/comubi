﻿using ComUbi.COMMON.Entidades;
using ComUbi.COMMON.Interfaces;
using ComUbi.DAL;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ComUbi.BIZ
{
    public class GenericManager<T> : IGenericManager<T> where T : BaseDTO
    {
        internal IGenericRepository<T> repository;
        public GenericManager(IGenericRepository<T> _repository)
        {
            repository = _repository;
        }
        public IEnumerable<T> Leer { get { return repository.Read; } }
        
        public string Error { get { return repository.Error; } }

        public T Actualizar(T entidad) => repository.Update(entidad);

        public T Agregar(T Entidad) => repository.Create(Entidad);

        public bool Eliminar(ObjectId Id) => repository.Delete(Id);
    }
}
