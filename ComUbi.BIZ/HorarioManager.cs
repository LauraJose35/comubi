﻿using ComUbi.COMMON.Entidades;
using ComUbi.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ComUbi.BIZ
{
    public class HorarioManager : GenericManager<Horario>, IHorarioManager
    {
        public HorarioManager(IGenericRepository<Horario> _repository) : base(_repository)
        {
        }
    }
}
